extends KinematicBody2D
class_name Player

export var gravity = 20
export var walking_speed = 20.0
export var running_speed = 20.0
export var sprinting_speed = 20.0

onready var animatedSprite = $sprite
onready var _player_state_machine = $state_machine

var _velocity = Vector2()

func _ready():

	# init state machine
	_player_state_machine = get_node("state_machine")
	_player_state_machine.init_player_state_machine(self) # must be abastract state machine instead
	_player_state_machine.connect("state_changed", self, "_on_state_changed")

func _input(event):
	_player_state_machine.handle_input(event)

func _process(delta):
	_player_state_machine.process_state(delta)

func _physics_process(delta):
	_player_state_machine.physics_process_state(delta)

func _on_state_changed(state_stack: Array):
	print("--------------")
	for i in state_stack.size():
		print("%d. %s" % [i, state_stack[i].name])