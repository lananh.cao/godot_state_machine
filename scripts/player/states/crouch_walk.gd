extends PlayerState

export(float) var speed = 100.0

func _enter(previous_state):
	._enter(previous_state)
	_player.animatedSprite.play("crouch_walk")

func _process_state(delta):
	._process_state(delta)
	if not _is_down_pressed:
		emit_signal("change_state", "previous")
	if _direction.x == 0:
		emit_signal("change_state", "previous")
	_player.position.x += _direction.x * delta * speed