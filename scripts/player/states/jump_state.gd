extends PlayerState

export(float) var gravity = 10
export(float) var jump_power = 250
var _velocity = Vector2(0, 0)

func _enter(previous_state):
	._enter(previous_state)
	_player.animatedSprite.play("jump")

func _physics_process_state(delta):
	#print("jump state called")
	_velocity.y += gravity
	_player.move_and_slide(_velocity)