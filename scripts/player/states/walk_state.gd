extends PlayerState

export(float) var speed = 200.0

func _enter(previous_state):
	._enter(previous_state)
	_player.animatedSprite.play("walk")

func handle_input(event: InputEvent):
	# print(_direction.x)
	pass
	
func _process_state(delta):
	._process_state(delta)
	if _direction.x == 0:
		emit_signal("change_state", "previous")
	if _is_down_pressed:
		emit_signal("change_state", "crouch")
	# print("%s" % Input.is_action_pressed("move_right"))
	_player.position.x += _direction.x * delta * speed

func _physics_process_state(delta):
	pass