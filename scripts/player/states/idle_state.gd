extends PlayerState

func _enter(previous_state):
	._enter(previous_state)
	_player.animatedSprite.play("idle")

func _process_state(delta):
	._process_state(delta)
	if _direction:
		emit_signal("change_state", "walk")
	if _is_down_pressed:
		emit_signal("change_state", "crouch")