extends PlayerState

func _enter(previous_state):
	._enter(previous_state)
	_player.animatedSprite.play("crouch")

func _process_state(delta):
	._process_state(delta)
	if _direction.x != 0:
		emit_signal("change_state", "crouch_walk")
	if _is_down_pressed == false:
		emit_signal("change_state", "previous")