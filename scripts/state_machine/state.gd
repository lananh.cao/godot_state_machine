extends Node

class_name State

signal change_state

var _state_machine = null

func _process_state(delta):
	return

func _physics_process_state(delta):
	return

# it will be either a player or node related
func _enter(previous_state):
	return

func _on_animation_finished(anim_name):
	return

func handle_input(event: InputEvent):
	return

func _exit(next_state):
	return