extends Node

class_name StateMachine

signal state_changed

var states_stack: Array = [null]
var current_state: State = null
var states_map = {}
var concurrent_states = [] # register which will be push down
var _previous_state := ""
var _next_state := "" # for queue change state on next loop

func init(initState: State):
#	states_stack.push_front(initState)
	ready(initState)

func _on_call_change_state(state_name: String, is_queue = false):
	# print("_on_call_change_state %s-%s" % [state_name, is_queue])
	if !is_queue:
		change_state(state_name)
	else:
		_next_state = state_name

# should be call be for ready
func add_state(state: State):
	var debug_log = "state_machine::add_state called %s exist[%s]" % [state.name, states_map.has(state.name)]
	print(debug_log)
	if !states_map.has(state.name):
		states_map[state.name] = state
		states_map[state.name].connect("change_state", self, "_on_call_change_state")
		states_map[state.name]._state_machine = self

func ready(initState: State):
	if initState != null && states_map.has(initState.name):
		change_state(initState.name)
	else:
		print("state has not register or is nil")

func handle_input(event: InputEvent):
	if current_state != null:
		var next_state = current_state.handle_input(event)
		
		if next_state != null:
			change_state(next_state)

func get_state_by_name(name: String, in_stack = true):
	if in_stack:
		for state in states_stack:
			if state.name == name:
				return state
		return null
	else:
		if states_map.has(name):
			return states_map[name]
	return null


# change state by name
# this is properly bad pratices
func change_state(stateName: String):
	print("change_state %s" % [stateName])
	if current_state != null:
		_previous_state = current_state.name
		current_state._exit(stateName)
	# special case for pop state
	if stateName == "previous":
		states_stack.pop_front()
	elif stateName in concurrent_states:
		states_stack.push_front(states_map[stateName])
	else:
		var state_to_change = states_map[stateName]
		states_stack[0] = state_to_change
	
	current_state = states_stack[0]
	if current_state != null:
		current_state._enter(_previous_state)
	emit_signal("state_changed", states_stack)

func process_state(delta):
	if current_state != null:
		current_state._process_state(delta)
	
	if _next_state != "":
		change_state(_next_state)
		_next_state = ""

func physics_process_state(delta):
	if current_state != null:
		current_state._physics_process_state(delta)